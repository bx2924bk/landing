<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Using constraints &amp; “magic numbers” to build responsive layouts in Figma</title>
    <link rel="icon" href="img/Medium-Avatar-500x500.svg">
    <style>-->
    figure {
        text-align: center;
    }

    div {
        padding: 10px 25%;
        font-size: 21px;
        line-height: 32px;
        font-family: charter, Georgia, Cambria, "Times New Roman", Times, serif;
    }
    </style>
</head>
<body>
<article>
    <h1 align="left" style="font-size: 48px; font-family: Georgia; padding: 10px 25% ">Using constraints &amp;“magic
        numbers” to build responsive layouts in Figma</h1>
</article>


<figure style="padding: 0; margin: 0;">

    <img src="img/face.png" alt="first img" width="100%">

    <figcaption>Image created for Figma by Peter Barnaba</figcaption>
</figure>


<div>
    <p>
        <em>This post is part of a series: </em>
        <a href="#"><em>Building a design system with Figma at littleBits</em></a>
        <em>. Read the previous post — how we created atomic components with Figma — </em>
        <a href="#"><em>here</em></a><em>.</em>
    </p>
    <p>This year we faced a big challenge at Little Bits — launching 4 new apps on multiple platforms with both mobile
        and tablet layouts. We knew we needed a strong design system to do it right, particularly given our small team
        and short time span (6 months).</p>
    <p>Templates with responsive layouts played a key role in scaling our work. Here’s how we figured out the approach
        that worked for us in both Figma and React.</p>
    <h1>Magic Number</h1>
    <p>
        <mark>One thing that’s great to have at the heart of a design system is a ‘magic number’ — it helps harmonize
            layout if sizes of text, graphic elements, margins, and padding, share a common numeric factor.
        </mark>
    </p>
    <p>Before we had any kind of system, we had mocks of several key screens that had been created to establish the
        visual direction of the app.</p>
</div>


<figure>

    <img alt="" src="img/iron_men.png" width="75%">

    <figcaption>
        Initial ‘eyeballed’ design mock compared with eventual design using layout based off 8's
    </figcaption>
</figure>

<div>
    <p>We looked at the sizes and padding that had been ‘eyeballed’ in these designs, and found that things were mostly
        working off 8’s — buttons were 48px high, margins were mostly 24px, some headings were 32px, etc.</p>
    <p>Everything was close enough to nudge towards numbers that multiplied from 8 (and 8 in itself is a number that
        multiplies and divides well!). So 8 became our magic number for all things moving forward.</p>
</div>
<figure>


    <img alt="" src="img/buttoms.png" width="50%">


    <figcaption>Buttons built on a scale of 8</figcaption>
</figure>
<div>
    <p>From smallest to largest our buttons and form elements were made 32, 48, 56, 64, &amp; 96px high; key headings
        &amp; copy 48, 32, 24, &amp; 16px; padding &amp; gutters 16, 24, &amp; 32px; larger layout cards would be 240px
        high.</p>
    <p>You get the picture…all the key sizes are multiples of 8!</p>
    <h1>Different Screen Sizes</h1>
    <p>Figma is great for building with responsive constraints — you can stretch your layouts and see how they will
        respond to changes in screen size. (If you’re new to constraints in Figma, check out
        <a href="#">this beginner primer</a> here.)</p>
    <p>You can even have elements pin to edges of a column:</p>
</div>


<figure>
    <img src="img/gif.gif" width="75%">
    <figcaption>Pin elements to a column in Figma using constraints</figcaption>
</figure>


<div>
    <p>But it’s hard to create a single layout that will look good on most phones, still fit on a smaller phone like an
        iPhone SE, and feel full on a tablet.</p>
</div>


<figure>
    <img alt="" src="img/controls.png" width="75%">
    <figcaption>Controls layouts on different screens with nothing but layout constraints.</figcaption>
</figure>


<div>
    <p>Around half our users are loading the app on a tablet, so we didn’t want the experience there to feel like an
        afterthought. We didn’t want our designs to be compromised to fit with smaller screens either.</p>
    <p>In this case our designs were sympathetic to being proportionally scaled up and down, so we looked at the
        relationship between different screen sizes to get scaling ratios:</p>
</div>
<figure>

    <img alt="" src="img/phones.png" width="50%">

</figure>
<div>
    <p>Then we rounded out the ratio values so we could keep whole numbers as we scaled:</p>
</div>
<figure>
    <img alt="" src="img/tables.png" width="50%">
</figure>
<div>
    <p>We baked these rules into how the React codebase calculated sizes (along with typical layout constraints).</p>
    <p>Because our templates were designed with this system in mind, when we tested the final build on tablets and
        smaller phones, things looked great right out of the box without needing extra adjustments.</p>
    <p>In our Figma designs we were able to have just one template design for each screen, and use a mixture of the
        scale tool and frame resizing to get a preview of how things would look on different devices.</p>
</div>


<figure>
    <img alt="" src="img/controls_2.png" width="75%">
    <figcaption>Controls layouts with constraints and scaling rules applied.</figcaption>
</figure>

<div>
    <p>There were other responsive design challenges to solve. The app had hundreds of videos and animations, we didn’t
        want to have to double handle multiple versions of the content files for different screen dimensions.</p>
    <p>We created all our video / animation content to work in 4:3 but with safe areas that could be cropped for 16:9
        screens.</p>
</div>
<figure>

    <img alt="" src="img/choped.png" width="50%">

    <figcaption>4:3 Content Cropped on 16:9</figcaption>
</figure>
<div>
    <h1>Text Sizes &amp; Internationalization</h1>
    <p>With a ‘magic number’, sometimes there will need to be exceptions — with text in particular at smaller sizes a
        few pixels makes a lot of difference. But where 8’s didn’t fit, we tried to fall back to 4’s — so some text
        sizes were 12 &amp; 20px.</p>
    <p>As well as needing variations for smaller copy, we were translating the app for 6 languages. Ideal text sizes
        were set in the templates for headings etc, but if a (German!) word didn’t fit within the available space, we
        created code to scale it down to the next heading size that would fit.</p>
</div>


<figure>

    <img alt="" src="img/hand.png" width="75%">
</figure>


<div>
    <p>So we ended up with a larger number of heading sizes than we used in the primary designs, but we kept a smooth
        transition of sizes when dropping from H1 to H6.</p>
    <h1>Summary</h1>
    <p>Most of our layouts are reasonably simple — centered elements, or 2–3 columns of content. We were lucky not to
        need responsive patterns besides the scaling system and basic layout constraints.</p>
    <p>The ‘magic number’ helps the design feel tight, it saves time when deciding how to position, size and space
        things, and of course it is much easier to code up a design that follows well defined consistent rules.</p>
    <p>Next step was to figure out the <a href="#">correct workflow for our themes</a>.</p>
    <p><em>This post is part of a series </em><a href="#"><em>Building a Design System with Figma at littleBits</em></a><em>.
        Read the next post </em><a href="#"><em>Creating Themed Versions of a Master Design in Figma</em></a><em>.</em>
    </p>
</div>


</body>
</html>